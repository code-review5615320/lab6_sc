# Minesweeper

## Functionality

+ you have a game field and you have to find all mines without any mistakes and during predefined period of time;
+ you can choose difficulty of the game that affects game field size, number of mines and period of time;
+ you can travel back in history during the game;
+ after loosing, wining or exceeding time history will be erased;
+ have fun and impove you brain :)

## Programming Principles

### Fail Fast

It tells us that we should fail fast our program if there is occured errors. 

We've used it in:
- [Grid](./src/Models/Grid.ts)
- [Game](./src/Models/Game.ts)

If user use these classes incorrectly or passes incorrect instance exception will be thrown.

### Open/Close Principle

It tells that our classes must be closed for changing but opened for extending.

We've used it in:
- [IGameState](./src/feature/types.ts)
- [IHistory](./src/feature/types.ts)

We can easily extend functionality without any changes in history or game components by implementing these interfaces 

### Single Responsibility principle

It tells that we should create components and unites them into modules if they changes at the same time and by the same reasons.

We've used it in:
- [Grid](./src/Models/Grid.ts)
- [Game](./src/Models/Game.ts)

These classes do only what they are supposed to, nothing more. Game.ts created play field, Grid.ts represents it, it is all.

### Composition Over Inheritance

It tells us that we should use interfaces and other abstraction over just inherit something.

We've used it in:
- [All state instances](./src/feature/constants.ts)
- [Grid](./src/Models/Grid.ts)

We've not used inheritance at all, instead we've used composition in state and history systems.

### You aren't gonna need it

Create only that for what you was asked 

We've created minimum functionality for satisfying work of our game.


## Design Patterns

### Prototype (Clone)

- [Grid.ts](./src/Models/Grid.ts)

That pattern was used for cloning of play field

### Singleton

- [Game](./src/Models/Game.ts)

That pattern was used for creating of game instance once what is very handy for the game. (I know that there can be problem with derived classes, but ablity of extending base class is more important for me than just privatize constructor. If that class will be extended by experienced programmer everything will be all right)

### Template method

- [Grid.ts](./src/Models/Grid.ts#L41)

That pattern was used for posibility of extending grid building process partially

### State

- [App](./src/Components/App.tsx)

That pattern was used for handy management of game state.

### Memento

- [History](./src/Components/History.tsx)

That pattern was used for creating history of user movements

### Observer

- [Timer](./src/Components/Timer.tsx)

That pattern was used for handy work with timer, that only one pattern in our game that is represented by library (rxjs)

## Refactoring Techniques

- Extract Method
- Inline Temp
- Inline Method
- Extract Subclass
- Rename Method
- Replace Error Code with Exception
