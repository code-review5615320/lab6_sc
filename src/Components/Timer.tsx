import { interval } from "rxjs";
import { useContext, useEffect, useState } from "react";
import { AppContext } from "../Context/AppContext";
import { abortedState, playingState } from "../feature/constants";
import { RevealAll } from "../feature/helper";

export default function Timer() {

    const { gameState, difficulty, setGameState, setSquares, squares } = useContext(AppContext)
    const [time, setTime] = useState(0)

    useEffect(() => {
        if (gameState === playingState) {
            setTime(difficulty.time);

            const sub = interval(1000).subscribe(_ => {
                setTime(t => {
                    if (t - 1 === 0) {
                        setGameState(abortedState)
                        RevealAll(squares.Data)
                        setSquares(squares.clone())
                    }

                    return t - 1
                })
            })

            return () => sub.unsubscribe()
        }
    }, [gameState, difficulty])

    return <div className = 'timer'>{time}</div>
}