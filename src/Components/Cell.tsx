import React, { useContext } from "react";
import "../styles.css";
import {Square} from "../Models/Squares/Square";
import {NumberSquare} from "../Models/Squares/NumberSquare";
import {BombSquare} from "../Models/Squares/BombSquare";
import { AppContext } from "../Context/AppContext";
import { toLooseState } from "../feature/constants";

export default function Cell({ square }: { square: Square }) {

    const { setGameState, setSquares, difficulty, squares, setHistory, history } = useContext(AppContext);

    const reveal = () => {
        if (square.isRevealed || square.isFlagged)
            return;


        // this logic should be written in the BombSquare.reveal() method,
        // and there already call a Game.Instance method similar to "GameLost"
        // to which the component  should already be subscribed and process it.
        // but while the state of the game instance is not read in any way,
        // it will remain like this

        if (square instanceof BombSquare){
            setGameState(toLooseState);
            return;
        }

        //

        setHistory([...history, squares.clone()]);
        square.reveal()
        setSquares(squares.clone());
    };

    const setFlag = (
        e: React.MouseEvent<HTMLDivElement>,
    ) => {
        e.preventDefault();
        if (square.isRevealed) return;

        square.setFlag();

        const newSquares = squares.clone();

        setSquares(newSquares);
    };

    const cellContent = () => {
        if (square.isFlagged && !square.isRevealed)
            return '🚩'
        return square.displaySymbol;
    }

    return <div
        className={`square square--${square.isRevealed && "revealed"}`}
        data-value={square instanceof NumberSquare ? square.neighborBombs : ""}
        onClick={() => reveal()}
        onContextMenu={(e) => setFlag(e)}
    >
        {cellContent()}
    </div>;
}