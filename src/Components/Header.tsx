import React, { useContext } from "react";
import { AppContext } from "../Context/AppContext";
import { baseDifficulty, hardDifficulty, middleDifficulty, toPlayState } from "../feature/constants";
import Timer from "./Timer";

export default function Header() {

    const { setDifficulty, setGameState, gameState } = useContext(AppContext)

    const difficulties = [
        { name: 'base', dif: baseDifficulty },
        { name: 'middle', dif: middleDifficulty },
        { name: 'hard', dif: hardDifficulty }]

    const onChangeSelect = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const diff = difficulties.find(el => el.name === e.target.value)
        if (!diff)
            return;

        setDifficulty(diff.dif)
        setGameState(toPlayState)
    }

    const onClickRestart = () => setGameState(toPlayState)

    return <div className="header">
        <div className="smile">
            {gameState.getSmile()}
        </div>
        <div className="menu">
            <button onClick={onClickRestart}>
                Restart
            </button>
            <select onChange={onChangeSelect}>
                {difficulties.map(el => <option value={el.name} key={el.name}>{el.name}</option>)}
            </select>
        </div>
        <div className="message">
            {gameState.getMessage()}
        </div>
        <Timer />
    </div>
}