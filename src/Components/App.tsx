import { useCallback, useEffect, useState } from "react";
import "../styles.css";
import { IHistory} from "../feature/types";
import { Game } from "../Models/Game";
import { baseDifficulty, playingState } from "../feature/constants";
import { AppContext } from "../Context/AppContext";
import Cell from "./Cell";
import { IGameState } from "../feature/types";
import Header from "./Header";
import History from "./History";
import Grid from "../Models/Grid";

export default function App() {

  const [difficulty, setDifficulty] = useState(baseDifficulty);
  const [gameState, setGameState] = useState<IGameState>(playingState)

  const init = useCallback(() => {
    Game.Instance.GridInfo = difficulty;
    return Game.Instance.BuildGame();
  }, [difficulty]);

  const [squares, setSquares] = useState<Grid>(init());
  const [history, setHistory] = useState<IHistory[]>([])

  const context = { init, setSquares, difficulty, squares, setDifficulty, gameState, setGameState, history, setHistory };

  // assign the newly initialized grid to the game
  Game.Instance.grid =  squares;

  useEffect(() => {
    gameState.handle(context)
  }, [gameState, squares])

  return (
    <AppContext.Provider value={context}>
      <div className="App">
        <Header />
        <div className="history-and-grid">
          <div>
            {squares.Data.map((row, rowIdx) => (
              <div key={rowIdx} className="row">
                {row.map((square) => (
                  <Cell key={`${square.x}-${square.y}`} square={square} />
                ))}
              </div>
            ))}
          </div>
          <History />
        </div>
      </div>
    </AppContext.Provider>
  );
}