import React, { useContext, useEffect, useState } from "react";
import { AppContext } from "../Context/AppContext";
import Grid from "../Models/Grid";

export default function History() {

    const { setSquares, history, setHistory } = useContext(AppContext)

    return <div className="history">
        {history.map((el, index) => {
            return <button key={index} onClick={() => {
                const grid = new Grid([])
                grid.restore(el)
                setSquares(grid)
                setHistory(history.slice(0, index))
            }}>back to {index + 1} step</button>
        })}
    </div>
}
