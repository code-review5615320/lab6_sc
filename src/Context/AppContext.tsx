import { createContext } from "react";
import { AppContextType } from "../feature/types";
import { baseDifficulty, playingState } from "../feature/constants";
import Grid from "../Models/Grid";

export const AppContext = createContext<AppContextType>({
    init: () => new Grid([]),
    setSquares: () => { },
    squares: new Grid([]),
    difficulty: baseDifficulty,
    setDifficulty: () => { },
    gameState: playingState,
    setGameState: () => { },
    history: [],
    setHistory: () => { },
})