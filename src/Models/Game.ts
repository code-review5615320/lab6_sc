import { GridInfo} from "../feature/types";
import Grid from "./Grid";

export class Game {
    private _gridInfo?: GridInfo
    private _grid?: Grid


    private static game: Game;


    public get grid(): Grid | undefined {
        return this._grid;
    }


    public set grid(grid: Grid | undefined) {
        this._grid = grid;
    }

    protected constructor() {
    }

    public static get Instance() {
        if (!this.game)
            this.game = new Game();

        return this.game;
    }

    public set GridInfo(value: GridInfo) {
        this._gridInfo = value;
    }

    public BuildGame() {
        if (!this._gridInfo)
            throw new Error('_Grid info must be initialized for game');

        this._grid = new Grid([])
        this._grid.GridInfo = this._gridInfo;

        this._grid.createBoard();
        return this._grid;
    }

}