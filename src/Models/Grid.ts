import { IClonable, IHistory, GridInfo } from "../feature/types";
import {Square} from "./Squares/Square";
import {EmptySquare} from "./Squares/EmptySquare";
import {BombSquare} from "./Squares/BombSquare";
import {NumberSquare} from "./Squares/NumberSquare";

export default class Grid implements IClonable<Grid>, IHistory {
    private squares: Square[][]
    private gridInfo?: GridInfo

    public set GridInfo(value: GridInfo) {
        this.gridInfo = value;
    }

    public constructor(squares: Square[][]) {
        this.squares = squares;
    }

    public get Data() {
        return this.squares;
    }

    public clone() {
        const newData = this.squares.map(row => {
            return row.map(cell => cell.clone())
        });
        let newGrid = new Grid(newData)
        newGrid.gridInfo = this.gridInfo;

        return newGrid;
    }

    public restore(history: IHistory) {
        if(!(history instanceof Grid))
            throw new Error("Incorrect type of history object")

        this.squares = history.Data;
        this.gridInfo = history.gridInfo
    }

    public createBoard(): void {
        if(!this.gridInfo)
            return
        this.InitializeCells();
        this.PlaceMines();
        this.InsertNumberCells();
    };

    protected InitializeCells(): void {
        if(!this.gridInfo)
            return

        this.squares = new Array(this.gridInfo.rows);

        for(let x = 0; x < this.gridInfo.rows; x++) {
            this.squares[x] = new Array(this.gridInfo.cols);
            for(let y = 0; y < this.gridInfo.cols; y++) {
                this.squares[x][y] = new EmptySquare(x, y);
            }
        }
    };

    protected PlaceMines(): void {
        if (!this.squares || !this.gridInfo) {
            throw new Error("Squares are not initialized");
        }

        let randNumbers = new Set<number>();

        while (randNumbers.size < this.gridInfo.bombs) {
            let number = Math.floor(Math.random() * this.gridInfo.rows * this.gridInfo.cols);
            randNumbers.add(number);
        }

        let randNumbersArray = Array.from(randNumbers);
        for(let i = 0; i < randNumbersArray.length; i++) {
            const num = randNumbersArray[i];
            const row = Math.floor((num / this.gridInfo.rows) % this.gridInfo.rows);
            const col = Math.floor(num % this.gridInfo.cols);
            if (this.squares)
                this.squares[row][col] = new BombSquare(row, col);
        }

    };

    protected InsertNumberCells(): void {
        if (!this.squares || !this.gridInfo) {
            throw new Error("Squares are not initialized");
        }

        for (let i = 0; i < this.squares.length; i++) {
            for (let j = 0; j < this.squares[0].length; j++) {
                if (!(this.squares[i][j] instanceof BombSquare)) {
                    const bombs = this.GetMinesAround(i, j);
                    if (bombs > 0)
                        this.squares[i][j] = new NumberSquare(i, j, bombs);
                }
            }
        }
    };

    protected GetMinesAround(x: number, y: number): number {
        if (!this.squares || !this.gridInfo) {
            throw new Error("Squares are not initialized");
        }

        let bombs = 0;
        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                const newRow = x + i;
                const newCol = y + j;
                if (
                    newRow >= 0 &&
                    newRow < this.gridInfo.rows &&
                    newCol >= 0 &&
                    newCol < this.gridInfo.cols &&
                    this.squares[newRow][newCol] instanceof BombSquare
                ) {
                    bombs++;
                }
            }
        }
        return bombs;
    };

    // Recursive method for revealing empty cells neighbors
    public revealEmptySquareNeighbors(x: number, y: number): void {
        if (!this.squares || !this.gridInfo) {
            throw new Error("Squares are not initialized");
        }

        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                const newRow = x + i;
                const newCol = y + j;
                if (
                    newRow >= 0 &&
                    newRow < this.gridInfo.rows &&
                    newCol >= 0 &&
                    newCol < this.gridInfo.cols &&
                    !(this.squares[newRow][newCol] instanceof BombSquare) &&
                    !this.squares[newRow][newCol].isRevealed
                ) {
                    this.squares[newRow][newCol].reveal();
                }
            }
        }
    }
}