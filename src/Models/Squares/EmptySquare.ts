import {Square} from "./Square";
import {Game} from "../Game";

export class EmptySquare extends Square {
    get displaySymbol() {
        return '';
    }

    override clone() {
        const clone = new EmptySquare(this.x, this.y);
        clone.isRevealed = this.isRevealed;
        clone.isFlagged = this.isFlagged;
        return clone;
    }

    override reveal() {
        if (!this.isRevealed) {
            this.isRevealed = true;
            if(Game.Instance.grid)
                Game.Instance.grid.revealEmptySquareNeighbors(this.x, this.y);
        }
    }
}