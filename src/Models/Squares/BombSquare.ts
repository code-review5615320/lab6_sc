import {Square} from "./Square";

export class BombSquare extends Square {
    get displaySymbol() {
        return this.isRevealed ? '💣' : '';
    }
    override clone() {
        const clone = new BombSquare(this.x, this.y);
        clone.isRevealed = this.isRevealed;
        clone.isFlagged = this.isFlagged;
        return clone;
    }
    override reveal() {

    }
}