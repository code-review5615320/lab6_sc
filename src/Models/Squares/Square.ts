import {IClonable} from "../../feature/types";

export abstract class Square implements IClonable<Square> {
    isRevealed: boolean;
    isFlagged: boolean;
    x: number;
    y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
        this.isRevealed = false;
        this.isFlagged = false;
    }

    abstract get displaySymbol(): string;
    abstract clone(): Square;
    abstract reveal(): void;

    setFlag() {
        if (!this.isRevealed)
            this.isFlagged = !this.isFlagged;
    }
}