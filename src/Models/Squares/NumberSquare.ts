import {Square} from "./Square";

export class NumberSquare extends Square {
    neighborBombs: number;

    constructor(x: number, y: number, neighborBombs: number) {
        super(x, y);
        this.neighborBombs = neighborBombs;
    }

    get displaySymbol() {
        return this.isRevealed ? this.neighborBombs.toString() : '';
    }

    override clone() {
        const clone = new NumberSquare(this.x, this.y, this.neighborBombs);
        clone.isRevealed = this.isRevealed;
        clone.isFlagged = this.isFlagged;
        return clone;
    }

    override reveal() {
        if(!this.isRevealed)
            this.isRevealed = true;
    }
}