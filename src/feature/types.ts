import React from "react";
import Grid from "../Models/Grid";

export type GridInfo = {
  rows: number,
  cols: number,
  bombs: number,
  time:number
}

export type AppContextType = {
  init: () => Grid,
  squares: Grid,
  setSquares: React.Dispatch<React.SetStateAction<Grid>>,
  difficulty: GridInfo,
  setDifficulty: React.Dispatch<React.SetStateAction<GridInfo>>,
  gameState: IGameState,
  setGameState: React.Dispatch<React.SetStateAction<IGameState>>,
  setHistory: React.Dispatch<React.SetStateAction<IHistory[]>>,
  history:IHistory[]
}

export interface IGameState {
  getSmile: () => string,
  handle: (context: AppContextType) => void,
  getMessage: () => string,
}

export interface IClonable<T> {
  clone(): T
}

export interface IHistory{
  
}