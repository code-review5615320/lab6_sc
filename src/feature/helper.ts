import {Square} from "../Models/Squares/Square";

export function RevealAll(squares: Square[][]) {
  squares.forEach(el => {
    el.forEach(cel => {
      cel.isRevealed = true;
    })
  })
}