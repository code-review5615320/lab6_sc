import { RevealAll } from "./helper";
import { GridInfo, IGameState } from "./types";

export const baseDifficulty: GridInfo = {
    rows: 10,
    cols: 10,
    bombs: 13,
    time: 300
}

export const middleDifficulty: GridInfo = {
    rows: 15,
    cols: 20,
    bombs: 55,
    time: 685
}

export const hardDifficulty: GridInfo = {
    rows: 15,
    cols: 25,
    bombs: 85,
    time: 999
}

export const toLooseState: IGameState = {
    getSmile() {
        return '😡'
    },
    handle(context) {
        context.setHistory([])
        RevealAll(context.squares.Data)
        context.setSquares(context.squares.clone());
        context.setGameState(looseState)
    },
    getMessage() {
        return 'You lost'
    }
}

export const looseState: IGameState = {
    getSmile() {
        return '😡'
    },
    handle(context) {
        context.setHistory([])
    },
    getMessage() {
        return 'You lost'
    }
}

export const wonState: IGameState = {
    getSmile() {
        return '😎'
    },
    handle(context) {
        context.setHistory([])
    },
    getMessage() {
        return 'You won'
    }
}

export const abortedState: IGameState = {
    getSmile() {
        return '😩'
    },
    handle(context) {
        context.setHistory([])
    },
    getMessage() {
        return 'Game has been aborted due to exceeding time limit'
    }
}

export const playingState: IGameState = {
    getSmile() {
        return '😗'
    },
    handle(context) {
        const revealed = context.squares.Data.reduce((acc, row) => {
            acc += row.reduce((acc2, sq) => {
                acc2 += sq.isRevealed ? 1 : 0;
                return acc2;
            }, 0);
            return acc;
        }, 0);

        if (revealed === (context.difficulty.rows * context.difficulty.cols - context.difficulty.bombs)) {
            RevealAll(context.squares.Data)
            context.setSquares(context.squares.clone())
            context.setGameState(wonState)
            return;
        }
    },
    getMessage() {
        return ''
    }
}

export const toPlayState: IGameState = {
    getSmile() {
        return '😗'
    },
    handle(context) {
        context.setHistory([])
        context.setSquares(context.init())
        context.setGameState(playingState);
    },
    getMessage() {
        return ''
    }
}